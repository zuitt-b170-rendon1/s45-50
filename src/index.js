/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/
// set up/import dependencies
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import App from './App.js'
//import {BrowserRouter, Route, Switch} from 'react-router-dom'; - Switch is depricated that's why we need to use Routes inside react

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';



ReactDOM.render(
  <App/>,
 document.getElementById('root')
);
