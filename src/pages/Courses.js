//base imports
import React from 'react'
import Course from '../components/Course';

//bootstrap dependencies
import Container from 'react-bootstrap/Container';

//data import
import courses from '../mock-data/courses';

export default function Courses(){
	const CourseCards = courses.map((course) =>{
		return (
			<Course course={course}/>
		)
	})
	return (
		<Container fluid>
		{CourseCards}
		</Container>
	)
}