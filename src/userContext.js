// dependencies
import React from 'react';

/*
	React.createContext()
	- create a Context Object that allows information storage within the app and pass it around the components

	with this, we'll be able to create global state to store user details instead of getting from loca storate from time to time

	a different approach to passing information between components w/o the use of props and pass it from parent to child since it is already being passed on to other components as a global state for the user

*/

export default React.createContext ();