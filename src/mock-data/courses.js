


export default  [
 {
 	id: "wdc001",
 	name: "PHP - LARAVEL",
 	description: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices tortor quis consectetur elementum. Sed ac nunc eleifend mauris auctor dapibus. Phasellus interdum, mi quis auctor elementum, nulla nulla hendrerit justo, in maximus tellus ipsum sed urna. Praesent sed rutrum sapien, quis fermentum mi.",
 	price: 45000,
 	onOffer: true
 },

 {
 	id: "wdc002",
 	name: "Python - Django",
 	description: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices tortor quis consectetur elementum. Sed ac nunc eleifend mauris auctor dapibus. Phasellus interdum, mi quis auctor elementum, nulla nulla hendrerit justo, in maximus tellus ipsum sed urna. Praesent sed rutrum sapien, quis fermentum mi.",
 	price: 50000,
 	oOffer: true
 },


 {
 	id: "wdc003",
 	name: "Java - Springboot",
 	description: " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices tortor quis consectetur elementum. Sed ac nunc eleifend mauris auctor dapibus. Phasellus interdum, mi quis auctor elementum, nulla nulla hendrerit justo, in maximus tellus ipsum sed urna. Praesent sed rutrum sapien, quis fermentum mi.",
 	price: 55000,
 	oOffer: true
 }
]
