// dependencies
import React from 'react';

// bootstrap components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';


export default function Highlights (){
	return (

		<Row>
	{/*xs and md - allows setting of breakpoints*/}
	<Col cs={12} md={4}>
		<Card className="card-highlight">
			<Card.Body>
				<Card.Title>
					<h2>Learn from Home</h2>
				</Card.Title>
				<Card.Text>
					Enjoy learning with the comfort of your room/home
				</Card.Text>
			</Card.Body>
		</Card>
	</Col>


	<Col cs={12} md={4}>
		<Card className="card-highlight">
			<Card.Body>
				<Card.Title>
					<h2>Study Now, Pay Later</h2>
				</Card.Title>
				<Card.Text>
					Never be bothered by the expenses while learning
				</Card.Text>
			</Card.Body>
		</Card>
	</Col>


	<Col cs={12} md={4}>
		<Card className="card-highlight">
			<Card.Body>
				<Card.Title>
					<h2>Be Part of Our Community</h2>
				</Card.Title>
				<Card.Text>
					Zuitt offers job for aspiring web developers from beginners to experienced who wants to refresh their minds on coding
				</Card.Text>
			</Card.Body>
		</Card>
	</Col>

	</Row>

	)
}

