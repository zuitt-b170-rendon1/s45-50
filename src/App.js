/*// Base Imports
import React, {useState} from 'react'; 

//import {BrowserRouter, Route, Switch} from 'react-router-dom'; - Switch is depricated that's why we need to use Routes inside react
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';*/
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2

  SPA - Single Page Application
   SPA - works like a magnetic board wherein the displays are the ones to be changed and not the board itself once there is a eed to change a theme

*/



/* 
  react-router-dom allows us to stimulate changing pages in react. Because by default, react is used for SPA - Single Page Application 

  Router - used to wrap components that uses react-router-dom and allows the use of routes and the routing system

  Routes - holds all the Route components (similar to the depricated Switch)

  Route - assigns an endpoint and displays the appropriate page component for that endpoint.
        - path attribute - assigns the endpoint
        -element attribute - assigns the Page component to be displayed at that endpoint (new update), before it was component
*/
/*
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import UserContext from './userContext.js'

// App Components

  every component has to be imported before it can be rendered inside the index.js

import AppNavbar from './components/AppNavbar.js';
*/
// Page Components
/*import Home from './pages/Home.js'
import Courses from './pages/Courses.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import PageNotFound from './pages/PageNotFound.js'
*/

//import Counter from './components/Counter.js';

/*
ReactDOM.render()
  responsible for injecting/inserting the whole React.js Project inside the webpage
*/
/*
  depricated version of the render() in react
*/
// React.createElement('h1', null, 'Hello World');

/*
  JSX - JavaScript XML - is an extention of JS that let's us create objects which will then be compiled and added as HTML elements

  With JSX..
    -we are able to create HTML elements using JS
    -we are also able to create JS objects that will then be compuiled and added as HTML elements

*/

/*
  Fragment - used to render different components inside the index.js. without it, the webpage will return errors since, it is the of JS to display two or more components in the frontend
    <>
    </>
      -also accepted in place of the Fragment but not all browsers are able to read this. also this does not support keys or attributes
*/
/*
npm start - to start/launch the react app
*/

/*
The provider component inside useContext is what allows other components to consume or use the context. Any component which is no wrapped by the provider will have access to the values provided in the context
*/


/*export default function App() {
  localStorage.getItem - used to retrieve a piece or the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.setItem in the login.js

  

  const [user, setUser] = useState({access: localStorage.getItem('access')});

const unsetUser = () => {
  //localStorage.clear() - to clear every info that is stored inside the localStorage
  localStorage.clear()
  setUser ({access: null})
}*/
// it wont logout



/* return (   
  <UserContext.Provider value = {{user, setUser, unsetUser}}>
 	<Router>
    <AppNavbar user={user}/> 
     <Routes>
      <Route exact path = "/" element={<Home/>}/> 
      <Route exact path = "/courses" element={<Courses/>}/> 
      <Route exact path = "/register" element={<Register/>}/> 
      <Route exact path = "/login" element={<Login/>}/> 
      <Route exact path = "*" element={<PageNotFound/>}/> 
     </Routes>  
   </Router>
   </UserContext.Provider>
  )
}*/


// Base Imports
import React, { useState } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom'; - Switch is depricated that's why we need to use Routes function inside react-router-dom instead
/* 
  react-router-dom allows us to simulate changing pages in react. Because by default, react is used for SPA - Single Page Application 
      SPA - works like a magnetic board wherein the displays are the ones to be changed and not the board itself once there is a need to change a theme

  Router - used to wrap components that uses react-router-dom and allows the use of routes and the routing system

  Routes - holds all the Route components (similar to the depricated Switch)

  Route - assigns an endpoint and displays the appropriate page component for that endpoint.
        - path attribute - assigns the endpoint
        - element attribute - assigns the Page component to be displayed at that endpoint (new update - before, it was component attribute)
*/
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// App Imports
import UserContext from './userContext.js';


// App Components
/*
  every component has to be imported before it can be rendered inside the index.js
  if the component is imported in App.js, all pages will display that component
*/
import AppNavbar from './components/AppNavbar.js';

// Page Components
import Home from './pages/Home.js';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
//import Error from './pages/Error'
import PageNotFound from './pages/PageNotFound.js';


export default function App() {
// localStorage.getItem - used to retrieve a piece or the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.setItem in the Login.js
  const [ user, setUser ] = useState( { accessToken: localStorage.getItem('access') } );

  const unsetUser = () => {
    //localStorage.clear() - to clear every information that is stored inside the localStorage
    localStorage.clear();
    setUser( { access: null } )
  }



/*
  path="*" - all other unspecified routes. this is to make sure that all other routes, beside the ones in the return statement, will render the Error page.
*/

/*
  The Provider component inside useContext is what allows other components to consume or use the context. Any component which is not wrapped by the Provider will have access to the values provided in the context.
*/

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
       <Router>
         <AppNavbar user={user} />
         <Routes>
             <Route path = "/" element={<Home />} />
             <Route path = "/courses" element = {<Courses />} />
             <Route path = "/register" element = {<Register />} />
             <Route path = "/login" element = {<Login />} />
        
              <Route exact path = "*" element={<PageNotFound/>}/> 
         </Routes>
      </Router>
    </UserContext.Provider>
    )
}






